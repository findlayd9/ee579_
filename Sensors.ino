//###########################################################
//                   **checkSensors()**
//      Function checks all IR and ultrasound sensors and 
//      responds appropriatly 
//###########################################################
bool last_turn;

void checkSensors(){
  
  int frontDist, rightDist, leftDist, backDist;
  bool frontRightIR;
  bool frontLeftIR;
  bool rearRightIR;
  bool rearLeftIR;
  
  //#########################
  //    CHECK FRONT ULTRASOUND SENSOR
  // #########################  
  frontDist = getDistance(FRONTULTRASOUND_TRIG, FRONTULTRASOUND_ECHO);
//  if(frontDist< 10){
//    //BRAKING--
//    DriveBackwards();
//    delay(300);
//  }
  //#########################
  //    CHECK BACK ULTRASOUND SENSOR
  // #########################  
  backDist = getDistance(BACKULTRASOUND_TRIG, BACKULTRASOUND_ECHO);
//  if(backDist< 30){
//    //RESPOND
//  }
  //#########################
  //    CHECK RIGHT SIDE ULTRASOUND SENSOR
  //#########################  
  rightDist = getDistance(RIGHTULTRASOUND_TRIG, RIGHTULTRASOUND_ECHO);
//  if(frontDist< 30){
//    //RESPOND
//  }
  //#########################
  //    CHECK LEFT SIDE ULTRASOUND SENSOR
  // #########################  
  leftDist = getDistance(LEFTULTRASOUND_TRIG, LEFTULTRASOUND_ECHO);
//  if(frontDist< 30){
//    //RESPOND
//  }
//  Serial.print("Front Ultrasound: ");
//  Serial.println(frontDist);
//  Serial.print("Right Ultrasound: ");
//  Serial.println(rightDist);
//  Serial.print("Left Ultrasound: ");
//  Serial.println(leftDist);
//  Serial.print("Back Ultrasound: ");
//  Serial.println(backDist);
//  Serial.print("\n");
    frontLeftIR = getIR(IR_FRONT_LEFT); //##### FRONT LEFT TRIGGERED
    frontRightIR = getIR(IR_FRONT_RIGHT); //##### FRONT RIGHT TRIGGERED
    rearLeftIR = getIR(IR_BACK_LEFT); //##### BACK LEFT TRIGGERED
    rearRightIR = getIR(IR_BACK_RIGHT); //##### BACK RIGHT TRIGGERED
    
    if (frontLeftIR == true && frontRightIR == true){
      // REVERSE, WHICH DIRECTION?
      if (last_turn = true){
        last_turn = BackwardsTurnLeft(100);
      }
      else{
        last_turn = BackwardsTurnRight(100);
      }
    }
    else if(rearLeftIR == true && frontLeftIR == true){
      // DRIVE FORWARDS, TURN RIGHT
      last_turn = ForwardsTurnRight(50);
    }
    else if(rearRightIR == true && frontRightIR == true){
      // DRIVE FORWARDS, TURN LEFT
      last_turn = ForwardsTurnLeft(50);
    }
    else if(frontLeftIR == true){
      // REVERSE, TURN LEFT
      last_turn = BackwardsTurnLeft(100);
    }
    else if(frontRightIR == true){
      last_turn = BackwardsTurnRight(100);
    }
    else if(rearLeftIR == true){
      // DRIVE FORWARDS, STRAIGHT
      DriveForwards();
      
      delay(300);
    }
    else if(rearRightIR == true){
      // REVERSE, TURN RIGHT
      last_turn = BackwardsTurnRight(100);
    }
    else if(frontDist < 10){
      // REVERSE, TURN BASED ON COMPASS
      if (getCompassReading() == 2){
        BackwardsTurnRight(100);
      }
      else if (getCompassReading() == 3){
        BackwardsTurnRight(100);
      }
      else{
        if (last_turn == true){
          BackwardsTurnLeft(100);   
         }
         else{
          BackwardsTurnRight(100);
         }
      }
    }
    else if(backDist < 10){
      // DRIVE FORWARDS, TURN BASED ON COMPASS
      if (getCompassReading() == 2){
        ForwardsTurnRight(50);
      }
      else if (getCompassReading() == 3){
        ForwardsTurnLeft(50);
      }
      else{
        if (last_turn == true){
          ForwardsTurnLeft(50);   
         }
         else{
          ForwardsTurnRight(50);
         }
      }
    }
    else if(leftDist < 10){
      // DRIVE FORWARDS, TURN RIGHT
      last_turn = ForwardsTurnRight(50);
    }
    else if(rightDist < 10){
      // DRIVE FORWARDS, TURN LEFT
      last_turn = ForwardsTurnLeft(50);
    }

    if (getCompassReading() == 1){
      if (last_turn == true){
        ForwardsTurnLeft(50);   
      }
      else{
        ForwardsTurnRight(50);
      }
    }

  //#########################
  //    CHECK FRONT LEFT IR SENSOR
  //#########################

  /*//#########################
  //    CHECK FRONT RIGHT IR SENSOR
  //#########################
  frontRightIR = getIR(IR_FRONT_RIGHT);
  if(frontRightIR == true){   //##### FRONT RIGHT TRIGGERED
    //Serial.println("Front right IR triggered");
    BackwardsTurnRight(100);
  }
  

  //#########################
  //    CHECK FRONT LEFT IR SENSOR
  //#########################
  frontLeftIR = getIR(IR_FRONT_LEFT); //##### FRONT LEFT TRIGGERED
  if(frontLeftIR == true){
    //Serial.println("Front Left IR triggered");
    BackwardsTurnLeft(100);
  }
  
  //#########################
  //    CHECK REAR LEFT IR SENSOR
  //#########################
  rearLeftIR = getIR(IR_REAR_LEFT); //##### FRONT LEFT TRIGGERED
  if(rearLeftIR == true){
    //Serial.println("Front Left IR triggered");
    BackwardsTurnLeft(100);
  }*/
}

//###########################################################
//                   **getDistance()**
//      Function obtains the distanace to the nearest object 
//      from a given ultrasound sensor.
//      Sensor selected by stating the trigger and echo pins 
//      of the choosen sensor. 
//###########################################################

int getDistance(int trigPin, int echoPin){
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance= duration*0.034/2;
  // Prints the distance on the Serial Monitor
//  Serial.print("Distance: ");
//  Serial.println(distance);
//  delay(100);
  return(distance);
}

//###########################################################
//                   **getIR()**
//      Function obtains the reading from the IR sensors. 
//      Input parameter is the pin number of the choosen
//      IR sensor. 
//      Function returns 'true' if a white line is detected.
//      Returns false otherwise. 
//###########################################################
bool getIR(int sensor){
  int sensorValue = analogRead(sensor);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5 / 4095.0);
//  if (sensor == IR_BACK_LEFT){
////    Serial.println(voltage);
//  }
  if (voltage < 4){
    return(true);
  }
  else{ 
    return(false);  
  }

}