//###############################################
//          **DRIVING FUNCTIONS**  
//          -DriveForwards()
//          -DriveBackwards()
//            -DriveStop()
//###############################################



//#########################################
//        SET DRIVING FUNCTIONS
//    Used to set the H-bridge configuration 
//    for a specific rear motor direction.
//    Functions are as:
//            -DriveForwards()
//            -DriveBackwards()
//            -DriveReset()
//#########################################
void DriveForwards(){
//  digitalWrite(ENABLE_12, HIGH);
  digitalWrite(DRIVE_1A, LOW);
  digitalWrite(DRIVE_2A, HIGH);
}
void DriveBackwards(){
//  digitalWrite(ENABLE_12, HIGH);
  digitalWrite(DRIVE_1A, HIGH);
  digitalWrite(DRIVE_2A, LOW);
}
void DriveReset(){
//  digitalWrite(ENABLE_12, LOW);
  digitalWrite(DRIVE_1A, LOW);
  digitalWrite(DRIVE_2A, LOW);  
}
void DriveBreak(){
//  digitalWrite(ENABLE_12, LOW);
  digitalWrite(DRIVE_1A, HIGH);
  digitalWrite(DRIVE_2A, HIGH);  
}


//###############################################
//          **TURNING FUNCTIONS**
//    Used to set the H-bridge configuration 
//    for a specific front motor direction(Steering).
//    Functions are as:  
//            -TurnLeft()
//            -TurnRight()
//            -TurnReset()
//###############################################
void TurnLeft(){
  digitalWrite(DRIVE_3A, HIGH);
  digitalWrite(DRIVE_4A, LOW);
}
void TurnRight(){
  digitalWrite(DRIVE_3A, LOW);
  digitalWrite(DRIVE_4A, HIGH);
}
void TurnReset(){
  digitalWrite(DRIVE_3A, LOW);
  digitalWrite(DRIVE_4A, LOW);
}

//#######################
//      DRIVE-STEER
//#######################


bool BackwardsTurnRight(){
  DriveBackwards();
  TurnRight();
  delay(700);
  TurnLeft();
  DriveForwards();
  delay(400);
  bool turn = false;
  return turn;
}
bool BackwardsTurnLeft(){
  DriveBackwards();
  TurnLeft();
  delay(700);
  TurnRight();
  DriveForwards();
  delay(400);
  bool turn = true;
  return turn;
  
}

bool ForwardsTurnLeft(){
  DriveForwards();
  TurnLeft();
  delay(500);
  bool turn = true;
  return turn;
}
bool ForwardsTurnRight(){
  DriveForwards();
  TurnRight();
  delay(500);
  bool turn = false;
  return turn;
}
