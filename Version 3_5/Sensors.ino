//###########################################################
//                   **checkSensors()**
//      Function checks all IR and ultrasound sensors and 
//      responds appropriatly 
//###########################################################
bool last_turn;

void checkSensors(){
  
  int frontDist, rightDist, leftDist, backDist;
  bool frontRightIR;
  bool frontLeftIR;
  bool rearRightIR;
  bool rearLeftIR;
  
  //#########################
  //    CHECK FRONT ULTRASOUND SENSOR
  // #########################  
  frontDist = getDistance(FRONTULTRASOUND_TRIG, FRONTULTRASOUND_ECHO);
  if(frontDist< 10){
    //BRAKING--
    DriveBackwards();
    delay(300);
  }
  //#########################
  //    CHECK BACK ULTRASOUND SENSOR
  // #########################  
  backDist = getDistance(BACKULTRASOUND_TRIG, BACKULTRASOUND_ECHO);
  if(backDist< 30){
    //RESPOND
  }
  //#########################
  //    CHECK RIGHT SIDE ULTRASOUND SENSOR
  //#########################  
  rightDist = getDistance(RIGHTULTRASOUND_TRIG, RIGHTULTRASOUND_ECHO);
  if(frontDist< 30){
    //RESPOND
  }
  //#########################
  //    CHECK LEFT SIDE ULTRASOUND SENSOR
  // #########################  
  leftDist = getDistance(LEFTULTRASOUND_TRIG, LEFTULTRASOUND_ECHO);
  if(frontDist< 30){
    //RESPOND
  }
  Serial.print("Front Ultrasound: ");
  Serial.println(frontDist);
  Serial.print("Right Ultrasound: ");
  Serial.println(rightDist);
  Serial.print("Left Ultrasound: ");
  Serial.println(leftDist);
  Serial.print("Back Ultrasound: ");
  Serial.println(backDist);
  Serial.print("\n");
    if(IR_FL_ISR_Flag == 1){
      // REVERSE, TURN LEFT
      last_turn = BackwardsTurnLeft();
      IR_FL_ISR_Flag = 0;
    }
    if(IR_FR_ISR_Flag == 1){
      last_turn = BackwardsTurnRight();
      IR_FR_ISR_Flag = 0;
    }
    if(IR_BR_ISR_Flag == 1){
      // DRIVE FORWARDS, STRAIGHT
      DriveForwards();
      delay(300);
      IR_BR_ISR_Flag = 0;
    }
    if(IR_BL_ISR_Flag == 1){
      DriveForwards();
      delay(300);
      IR_BL_ISR_Flag = 0;
    }
    if(frontDist < 10){
      // REVERSE, TURN BASED ON COMPASS
//      if (getCompassReading() == 2){
//        BackwardsTurnRight();
//      }
//      else if (getCompassReading() == 3){
//        BackwardsTurnRight();
//      }
//      else{
        if (last_turn == true){
          BackwardsTurnLeft();   
         }
         else{
          BackwardsTurnRight();
         }
         
//      }
    }
//    if(backDist < 10){
//      // DRIVE FORWARDS, TURN BASED ON COMPASS
//      if (getCompassReading() == 2){
//        ForwardsTurnRight();
//      }
//      else if (getCompassReading() == 3){
//        ForwardsTurnLeft();
//      }
//      else{
//        if (last_turn == true){
//          ForwardsTurnLeft();   
//         }
//         else{
//          ForwardsTurnRight();
//         }
//      }
//    }
//    if(leftDist < 10){
//      // DRIVE FORWARDS, TURN RIGHT
//      last_turn = ForwardsTurnRight();
//    }
//    if(rightDist < 10){
//      // DRIVE FORWARDS, TURN LEFT
//      last_turn = ForwardsTurnLeft();
//    }

//    if (getCompassReading() != 1){
//      if (last_turn == true){
//        ForwardsTurnLeft();   
//      }
//      else{
//        ForwardsTurnRight();
//      }
//    }

//    if (getCompassReading() != 1){
//      if (last_turn == true){
//        ForwardsTurnLeft(50);   
//      }
//      else{
//        ForwardsTurnRight(50);
//      }
//    }

  //#########################
  //    CHECK FRONT LEFT IR SENSOR
  //#########################

  /*//#########################
  //    CHECK FRONT RIGHT IR SENSOR
  //#########################
  frontRightIR = getIR(IR_FRONT_RIGHT);
  if(frontRightIR == true){   //##### FRONT RIGHT TRIGGERED
    //Serial.println("Front right IR triggered");
    BackwardsTurnRight(100);
  }
  

  //#########################
  //    CHECK FRONT LEFT IR SENSOR
  //#########################
  frontLeftIR = getIR(IR_FRONT_LEFT); //##### FRONT LEFT TRIGGERED
  if(frontLeftIR == true){
    //Serial.println("Front Left IR triggered");
    BackwardsTurnLeft(100);
  }
  
  //#########################
  //    CHECK REAR LEFT IR SENSOR
  //#########################
  rearLeftIR = getIR(IR_REAR_LEFT); //##### FRONT LEFT TRIGGERED
  if(rearLeftIR == true){
    //Serial.println("Front Left IR triggered");
    BackwardsTurnLeft(100);
  }*/
}

//###########################################################
//                   **getDistance()**
//      Function obtains the distanace to the nearest object 
//      from a given ultrasound sensor.
//      Sensor selected by stating the trigger and echo pins 
//      of the choosen sensor. 
//###########################################################

int getDistance(int trigPin, int echoPin){
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance= duration*0.034/2;
  // Prints the distance on the Serial Monitor
//  Serial.print("Distance: ");
//  Serial.println(distance);
//  delay(100);
  return(distance);
}

//###########################################################
//                   **getIR()**
//      Function obtains the reading from the IR sensors. 
//      Input parameter is the pin number of the choosen
//      IR sensor. 
//      Function returns 'true' if a white line is detected.
//      Returns false otherwise. 
//###########################################################
bool getIR(int sensor){
  int sensorValue = analogRead(sensor);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5 / 4095.0);
  
  if (sensor == IR_FRONT_RIGHT){
    Serial.println(sensorValue);
  }
  if (voltage < 4){
    return(true);
  }
  else{ 
    return(false);  
  }

}
