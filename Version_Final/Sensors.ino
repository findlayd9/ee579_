//###########################################################
//                   **checkSensors()**
//      Function checks all IR and ultrasound sensors and 
//      responds appropriatly 
//###########################################################
bool last_turn;

void checkSensors(){
  
  int frontDist, rightDist, leftDist, backDist;
  bool frontRightIR;
  bool frontLeftIR;
  bool rearRightIR;
  bool rearLeftIR;
  
  //#########################
  //    CHECK FRONT ULTRASOUND SENSOR
  // #########################  
  frontDist = getDistance(FRONTULTRASOUND_TRIG, FRONTULTRASOUND_ECHO);
  if(frontDist< 20){
    //BRAKING--
    BackwardsTurnRight(50);
  }
  //#########################
  //    CHECK BACK ULTRASOUND SENSOR
  // #########################  
  backDist = getDistance(BACKULTRASOUND_TRIG, BACKULTRASOUND_ECHO);
  if(backDist< 30){
    //RESPOND
  }
  //#########################
  //    CHECK RIGHT SIDE ULTRASOUND SENSOR
  //#########################  
  rightDist = getDistance(RIGHTULTRASOUND_TRIG, RIGHTULTRASOUND_ECHO);
  if(frontDist< 30){
    TurnLeft();
  }
  //#########################
  //    CHECK LEFT SIDE ULTRASOUND SENSOR
  // #########################  
  leftDist = getDistance(LEFTULTRASOUND_TRIG, LEFTULTRASOUND_ECHO);
  if(frontDist< 30){
    TurnRight();
  }
    //frontLeftIR = getIR(IR_FRONT_LEFT); //##### FRONT LEFT TRIGGERED
    //frontRightIR = getIR(IR_FRONT_RIGHT); //##### FRONT RIGHT TRIGGERED
//    rearLeftIR = getIR(IR_BACK_LEFT); //##### BACK LEFT TRIGGERED
//    rearRightIR = getIR(IR_BACK_RIGHT); //##### BACK RIGHT TRIGGERED
    
//    if (frontLeftIR == true && frontRightIR == true){
//      // REVERSE, WHICH DIRECTION?
//      if (last_turn = true){
//        last_turn = BackwardsTurnLeft(120);
//      }
//      else{
//        last_turn = BackwardsTurnRight(120);
//      }
//    }
//    if(rearLeftIR == true && frontLeftIR == true){
//      // DRIVE FORWARDS, TURN RIGHT
//      last_turn = ForwardsTurnRight(50);
//    }
//    if(rearRightIR == true && frontRightIR == true){
//      // DRIVE FORWARDS, TURN LEFT
//      last_turn = ForwardsTurnLeft(50);
//    }
    if(analogRead(IR_FRONT_LEFT) < 1000){
      // REVERSE, TURN LEFT
      last_turn = BackwardsTurnLeft(120);
    }
    if(analogRead(IR_FRONT_RIGHT) < 1000){
      last_turn = BackwardsTurnRight(120);
    }
//    if(rearLeftIR == true){
//      // DRIVE FORWARDS, STRAIGHT
//      DriveForwards();
//      
//      delay(300);
//    }
//    if(rearRightIR == true){
//      // REVERSE, TURN RIGHT
//      last_turn = BackwardsTurnRight(120);
//    }
//    if(frontDist < 10){
//      // REVERSE, TURN BASED ON COMPASS
//      if (getCompassReading() == 2){
//        BackwardsTurnRight(120);
//      }
//      else if (getCompassReading() == 3){
//        BackwardsTurnRight(120);
//      }
//      else{
//        if (last_turn == true){
//          BackwardsTurnLeft(120);   
//         }
//         else{
//          BackwardsTurnRight(120);
//         }
//      }
//    }
//    if(backDist < 10){
//      // DRIVE FORWARDS, TURN BASED ON COMPASS
//      if (getCompassReading() == 2){
//        ForwardsTurnRight(50);
//      }
//      else if (getCompassReading() == 3){
//        ForwardsTurnLeft(50);
//      }
//      else{
//        if (last_turn == true){
//          ForwardsTurnLeft(50);   
//         }
//         else{
//          ForwardsTurnRight(50);
//         }
//      }
//    }
//    if(leftDist < 10){
//      // DRIVE FORWARDS, TURN RIGHT
//      last_turn = ForwardsTurnRight(50);
//    }
//    if(rightDist < 10){
//      // DRIVE FORWARDS, TURN LEFT
//      last_turn = ForwardsTurnLeft(50);
//    }

//    if (getCompassReading() != 1){
//      if (last_turn == true){
//        ForwardsTurnLeft(50);   
//      }
//      else{
//        ForwardsTurnRight(50);
//      }
//    }

 
}

//###########################################################
//                   **getDistance()**
//      Function obtains the distanace to the nearest object 
//      from a given ultrasound sensor.
//      Sensor selected by stating the trigger and echo pins 
//      of the choosen sensor. 
//###########################################################

int getDistance(int trigPin, int echoPin){
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance= duration*0.034/2;
  // Prints the distance on the Serial Monitor
//  Serial.print("Distance: ");
//  Serial.println(distance);
//  delay(100);
  return(distance);
}

//###########################################################
//                   **getIR()**
//      Function obtains the reading from the IR sensors. 
//      Input parameter is the pin number of the choosen
//      IR sensor. 
//      Function returns 'true' if a white line is detected.
//      Returns false otherwise. 
//###########################################################
bool getIR(int sensor){
  int sensorValue = analogRead(sensor);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5 / 4095.0);
  
  if (sensor == IR_FRONT_RIGHT){
    Serial.println(sensorValue);
  }
  if (voltage < 4){
    return(true);
  }
  else{ 
    return(false);  
  }

}
