
#include "MPU9250.h"


//###############################################
//            DEFINTIONS FOR PINS
//###############################################

//#####################################
//#########   ULTRASOUND SENSORS
#define FRONTULTRASOUND_TRIG  PB3
#define FRONTULTRASOUND_ECHO  PB8
#define RIGHTULTRASOUND_TRIG  PB5
#define RIGHTULTRASOUND_ECHO  PB4
#define LEFTULTRASOUND_TRIG   PB0
#define LEFTULTRASOUND_ECHO   PB1
#define BACKULTRASOUND_TRIG   PB10
#define BACKULTRASOUND_ECHO   PB11

//###########################
//        TIMER
//##########################

#define TIMERCLOCK_PERIOD 1 //microseconds
#define PWM_FREQUECNY  5000    //5kHz

//#####################################
//#########   IR SENSORS
#define IR_FRONT_RIGHT  PA0
#define IR_FRONT_LEFT   PA1
#define IR_BACK_RIGHT   PA2
#define IR_BACK_LEFT    PA3

//#####################################
//#########    MOTORS

//#########   Rear motor

#define ENABLE_12 PB12  //STM
#define DRIVE_1A PB13   //STM
#define DRIVE_2A PB14   //STM

//##########  Front motor
//

#define ENABLE_34 PB15  //STM
#define DRIVE_3A PA8   //STM
#define DRIVE_4A PA9   //STM

//#########   Compass
#define COMPASSI2CADRESS 0x68

// an MPU9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68
MPU9250 IMU(Wire,COMPASSI2CADRESS);
int status;


//######################
//  Variable declarations
//######################


//############
//    PWM
//############
float d = 0.7;    //50% Duty cycle 

//Global Counters
int ton_counter = 0;      //Initilise to zero
int T_counter = 0;        //Initilise to zero

//Set the max counter valaues depending on the PWM frquecny and duty cycle
float PWM_PERIOD = 1000000/PWM_FREQUECNY;          //Multiply by 1e6 to get in microseconds

float T_counter_Max = PWM_PERIOD/TIMERCLOCK_PERIOD;         //Counter value to obtain a switching period of fs
float ton_counter_Max = (T_counter_Max)*d;                  //Counter value to obtain a ton time of T*d

bool PWM_ENABLED = true;        

// defines variables
long duration;
int distance;

void setup() {
  pinMode(PC13, OUTPUT);      //Init finished LED
  digitalWrite(PC13, HIGH);
  
  pinMode(FRONTULTRASOUND_TRIG, OUTPUT); // Sets the trigPin as an Output
  pinMode(FRONTULTRASOUND_ECHO, INPUT); // Sets the echoPin as an Input
  pinMode(RIGHTULTRASOUND_TRIG, OUTPUT); // Sets the trigPin as an Output
  pinMode(RIGHTULTRASOUND_ECHO, INPUT); // Sets the echoPin as an Input
  pinMode(LEFTULTRASOUND_TRIG, OUTPUT); // Sets the trigPin as an Output
  pinMode(LEFTULTRASOUND_ECHO, INPUT); // Sets the echoPin as an Input
  pinMode(BACKULTRASOUND_TRIG, OUTPUT); // Sets the trigPin as an Output
  pinMode(BACKULTRASOUND_ECHO, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication
  
  //-- DRIVE SETUP---
  pinMode(ENABLE_12, OUTPUT);       //Rear motor(power) enable pin
  pinMode(DRIVE_1A, OUTPUT);
  pinMode(DRIVE_2A, OUTPUT);
  pinMode(ENABLE_34, OUTPUT);       //Front motor(steering)enable pin
  pinMode(DRIVE_3A, OUTPUT);
  pinMode(DRIVE_4A, OUTPUT);
  
  
  //--TIMER INITILISE--
  
  Timer2.setChannel1Mode(TIMER_OUTPUTCOMPARE);
  Timer2.setPeriod(TIMERCLOCK_PERIOD);                      // in microseconds
  Timer2.setCompare1(1);                                     // overflow might be small
  Timer2.attachCompare1Interrupt(timerInterruptHandler);    //Attach given interrupt to the timer

//  --COMPASS SETUPS--
//   serial to display data
  Serial.begin(9600);
  while(!Serial) {}

  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while(1) {}
  }

  DriveReset();
  TurnReset();
  digitalWrite(PC13, LOW);

}

//##################################################################################
//                          **timerInterruptHandler()**
//      Function drives the enable of the H-bridge of the rear
//      (front = steering, rear = power) motor controller using the timer interrupt in 
//      order to reduce speed. (Does PWM of the output signal)
//      The duty cycle of the PWM signal is defined at the top of EE579_MASTER_2.  
//      50% duty cycle (0.5 )means 50% of Vbattery etc.
//##################################################################################

void timerInterruptHandler(void) {
  ton_counter++;
  T_counter++;
  if(PWM_ENABLED == true){
    if(T_counter >= T_counter_Max){    //End of 1 PWM cycle
      ton_counter = 0;                 //Reset the ton_counter 
      T_counter = 0;                   //Reset the ton_counter 
      digitalWrite(ENABLE_12, HIGH);   //Turn the rear motor on
      digitalWrite(ENABLE_34, LOW);    //Turn the front motor on 
    }
    else if(ton_counter >=ton_counter_Max){   //PWM cycle not ended but ton period ended
      digitalWrite(ENABLE_12, LOW);
      digitalWrite(ENABLE_34, HIGH); 
    }
  }
} 

//##################################################################################
//                                  **main loop**
//##################################################################################
void loop() {
  DriveForwards();
  reverseRight();
  checkSensors();
}
