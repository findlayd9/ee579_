//###############################################
//          **DRIVING FUNCTIONS**  
//          -DriveForwards()
//          -DriveBackwards()
//            -DriveStop()
//###############################################



//#########################################
//        SET DRIVING FUNCTIONS
//    Used to set the H-bridge configuration 
//    for a specific rear motor direction.
//    Functions are as:
//            -DriveForwards()
//            -DriveBackwards()
//            -DriveReset()
//#########################################
void DriveForwards(){
//  digitalWrite(ENABLE_12, HIGH);
  digitalWrite(DRIVE_1A, LOW);
  digitalWrite(DRIVE_2A, HIGH);
}
void DriveBackwards(){
//  digitalWrite(ENABLE_12, HIGH);
  digitalWrite(DRIVE_1A, HIGH);
  digitalWrite(DRIVE_2A, LOW);
}
void DriveReset(){
//  digitalWrite(ENABLE_12, LOW);
  digitalWrite(DRIVE_1A, LOW);
  digitalWrite(DRIVE_2A, LOW);  
}
void DriveBreak(){
//  digitalWrite(ENABLE_12, LOW);
  digitalWrite(DRIVE_1A, HIGH);
  digitalWrite(DRIVE_2A, HIGH);  
}


//###############################################
//          **TURNING FUNCTIONS**
//    Used to set the H-bridge configuration 
//    for a specific front motor direction(Steering).
//    Functions are as:  
//            -TurnLeft()
//            -TurnRight()
//            -TurnReset()
//###############################################
void TurnLeft(){
  digitalWrite(DRIVE_3A, HIGH);
  digitalWrite(DRIVE_4A, LOW);
}
void TurnRight(){
  digitalWrite(DRIVE_3A, LOW);
  digitalWrite(DRIVE_4A, HIGH);
}

//Springs in wheel arch reset the cars steering after power is no longer applied.
void TurnReset(){
  digitalWrite(DRIVE_3A, LOW);
  digitalWrite(DRIVE_4A, LOW);
}

//###############################################
//          **DRIVE-STEER FUNCTIONS**
//    Used in combination with the above drive and 
//    steering functions to create specific driving
//    instructions for certain situations.
//    Functions are as:  
//            -BackwardsTurnRight()
//            -BackwardsTurnLeft()
//            -ForwardsTurnLeft()
//            -ForwardsTurnRight()
//###############################################


bool BackwardsTurnRight(int period){
  TurnRight();
  delay(1);
  DriveBackwards();
  delay(1100);
  TurnLeft();
  delay(300);
  DriveForwards();
  delay(500);
  bool turn = false;
  return turn;
}


bool BackwardsTurnLeft(int period){
  DriveBackwards();
  TurnLeft();
  delay(1200);
  TurnRight();
  DriveForwards();
  delay(500);
  bool turn = true;
  return turn;
  
}

bool ForwardsTurnLeft(int period){
  DriveForwards();
  TurnLeft();
  delay(500);
  bool turn = true;
  return turn;
}
bool ForwardsTurnRight(int period){
  DriveForwards();
  TurnRight();
  delay(500);
  bool turn = false;
  return turn;
}
