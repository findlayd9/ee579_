
//##################################################################################
//                          **getCompassReading()**
//      Function used to obtain the cars bearing using the MPU9250 chip.
//      This chip returns x,y and z magnitudes for the the bearing. This is calibrated
//      and used as a referenece point so that the intended direction can be set and 
//      aimed for. The function returns directional feedback commands depending on the cars 
//      orientation and the set desired direction.
//##################################################################################
int getCompassReading() {
  // read the sensor
  float heading;
  float newhead;
  IMU.readSensor();
  heading = (atan2(IMU.getMagY_uT(), IMU.getMagX_uT()) * 180 / 3.14159);
  newhead = heading - 36.0;
  if (newhead > -7.0 && newhead < 7.0){
//    Serial.println("Finish line");
    return(0);
  }
  else if (newhead > 90.0 or newhead < -90.0){
//    Serial.println("Wrong way");
    return(1);
  }
  else if (newhead < 90.0 && newhead > 7.0){
//    Serial.println("Turn Right");
    return(2);
  }
  else if (newhead > -90.0 && newhead < -7.0){
//    Serial.println("Turn Left");
    return(3);
  }
  else{
//    Serial.println("Unknown"); 
    return(255);
  }
}
